#! /usr/bin/python
#Daniel McCarthy 
#Solution for "Cosine Similarities" IEEEXtreme Sample
#http://www.ieee.org/membership_services/membership/students/awards/xtremesamples.html

import sys
from math import acos

def parseVect(vectStr):
    vectStr = vectStr.strip('[]')
    vect = vectStr.split(',')
    vect = [value for value in vect if value != '']
    vect = list(map(int, vect))
    return vect

def dotProd(vectA, vectB):
    ret = 0
    for i in range(0, len(vectA)):
        ret += vectA[i] * vectB[i]
    return ret

def vectMag(vect):
    sumsq = 0
    for i in vect:
        sumsq += i*i
    return sumsq**0.5


if len(sys.argv) < 3:
    print("Error")
else:
    vectA = parseVect(sys.argv[1])
    vectB = parseVect(sys.argv[2])
    if len(vectA) != len(vectB):
        print("Error")
    else:
        AdB = dotProd(vectA, vectB)
        magA = vectMag(vectA)
        magB = vectMag(vectB)
        magProd = magA*magB
        if magProd == 0:
            print("Error")
        else:
            print(acos(AdB/(magProd)))
