#! /usr/bin/python
#Daniel McCarthy
#Solution for "Telephone Keyboard Input Recognition" IEEEXtreme Sample
#http://www.ieee.org/membership_services/membership/students/awards/xtremesamples.html

import sys

def NumStr(string):
    nums = ""
    for char in string:
        nums += GetNum(char)
    return nums

def GetNum(char):
    if char.upper() in ['A', 'B', 'C']:
        return '2'
    if char.upper() in ['D', 'E', 'F']:
        return '3'
    if char.upper() in ['G', 'H', 'I']:
        return '4'
    if char.upper() in ['J', 'K', 'L']:
        return '5'
    if char.upper() in ['M', 'N', 'O']:
        return '6'
    if char.upper() in ['P', 'Q', 'R', 'S']:
        return '7'
    if char.upper() in ['T', 'U', 'V']:
        return '8'
    if char.upper() in ['W', 'X', 'Y', 'Z']:
        return '9'
    return '' #For non characters

if len(sys.argv) < 3:
    print("Error")
else:
    try:
        f = open(sys.argv[1], 'r')
    except:
        print("Couldn't open file :(")
        sys.exit()
    words = []
    while 1:
        strIn = f.readline()
        if strIn == '':
            break
        strIn = strIn.strip('\n')
        if sys.argv[2] in NumStr(strIn):
            words.append(strIn)
    wnum = len(words)
    if wnum == 0:
        print("No Matches")
    elif wnum == 1:
        print(words[0])
    else:
        print("{} matches:\n".format(wnum))
        for word in words:
             print(word)
